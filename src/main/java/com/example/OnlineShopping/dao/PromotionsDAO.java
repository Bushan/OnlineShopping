package com.example.OnlineShopping.dao;

import com.example.OnlineShopping.domains.Promotions;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PromotionsDAO {

    List<Promotions> promotionsList = new ArrayList<>();

    public PromotionsDAO(){
        promotionsList.add(new Promotions(1001,"Audio","30% discount"));
        promotionsList.add(new Promotions(1002,"AAA Batteries","3 for 2"));
    }

    public List<Promotions> getPromotionsList() {
        return promotionsList;
    }
}
