package com.example.OnlineShopping.dao;

import com.example.OnlineShopping.domains.ShoppingCart;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ShoppingCartDAO {

    List<ShoppingCart> shoppingList = new ArrayList<>();

    public ShoppingCartDAO(){
        shoppingList.add(new ShoppingCart(1001,"Headphones",1));
        shoppingList.add(new ShoppingCart(1002,"Speakers",1));
    }

    /**
     * Add item into the shopping cart
     * @param cart
     */
    public void addItemIntoCart(final ShoppingCart cart){
        shoppingList.add(cart);
    }

    /**
     * Removes the item from shopping cart
     * @param cart
     */
    public void removeItemIntoCart(final ShoppingCart cart){
        if(null!=cart)
            shoppingList.remove(cart);
    }

    /**
     * List of the shopping cart
     * @return
     */
    public List<ShoppingCart> getShoppingList(){
        return this.shoppingList;
    }

}
