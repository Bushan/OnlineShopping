package com.example.OnlineShopping.dao;

import com.example.OnlineShopping.domains.Items;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemDAO {

    public List<Items> getItems() {

        return fetchItems();
    }

    private List<Items> fetchItems() {
        Items item1 = new Items(1001,"Headphones","Audio", 150.00);
        Items item2 = new Items(1002,"Speakers","Audio", 85.00);
        Items item3 = new Items(1003,"AA Batteries","Power", 3.00);
        Items item4 = new Items(1004,"Protein","Food", 25.00);

        List<Items> items = new ArrayList<>();
        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);

        return items;
    }
}
