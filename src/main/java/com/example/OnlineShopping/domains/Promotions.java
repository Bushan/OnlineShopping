package com.example.OnlineShopping.domains;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class Promotions {
    //Item id
    private long id;
    // Item name
    private String itemName;
    //Discount details
    private String discount;

    public Promotions(){}

    public Promotions(long id, String itemName, String discount){
        this.discount=discount;
        this.id=id;
        this.itemName=itemName;
    }
}
