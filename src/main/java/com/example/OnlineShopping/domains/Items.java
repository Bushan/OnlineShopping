package com.example.OnlineShopping.domains;

import lombok.*;

import java.io.Serializable;

@Getter @Setter
public class Items implements Serializable {

    //Unique item id
    private long id;
    //Item name
    private String itemName;
    //Price of the item
    private double price;
    // Item type
    private String itemType;

    //A default constructor
    public Items(){}
    public Items(long id,String itemName, String itemType, double price){
        this.id=id;
        this.itemName = itemName;
        this.price = price;
        this.itemType = itemType;
    }

}
