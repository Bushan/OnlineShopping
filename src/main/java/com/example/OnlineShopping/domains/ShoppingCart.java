package com.example.OnlineShopping.domains;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ShoppingCart {

    //unique item id
    private long id;

    //Shopping cart item
    private String itemName;
    //Quantity
    private int quantity;

    //Default constructor
    public ShoppingCart(){}
    public ShoppingCart(long id, String itemName, int quantity){
        this.id=id;
        this.itemName=itemName;
        this.quantity=quantity;
    }
}
