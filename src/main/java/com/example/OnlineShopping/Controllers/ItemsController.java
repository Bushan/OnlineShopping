package com.example.OnlineShopping.Controllers;

import com.example.OnlineShopping.dao.ItemDAO;
import com.example.OnlineShopping.dao.PromotionsDAO;
import com.example.OnlineShopping.dao.ShoppingCartDAO;
import com.example.OnlineShopping.domains.Items;
import com.example.OnlineShopping.domains.Promotions;
import com.example.OnlineShopping.domains.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RestController
public class ItemsController {

    @Autowired
    ItemDAO itemDAO;

    @Autowired
    ShoppingCartDAO shoppingCartDAO;

    @Autowired
    PromotionsDAO promotionsDAO;

    public void setPromotionsDAO(PromotionsDAO promotionsDAO) {
        this.promotionsDAO = promotionsDAO;
    }

    public void setItemDAO(ItemDAO itemDAO) {
        this.itemDAO = itemDAO;
    }

    public void setShoppingCartDAO(ShoppingCartDAO shoppingCartDAO) {
        this.shoppingCartDAO = shoppingCartDAO;
    }

    public List<Items> getItems(){
        return itemDAO.getItems();
    }

    /**
     * Cart list
     * @return
     */
    public List<ShoppingCart> getShoppingList(){
        return  shoppingCartDAO.getShoppingList();
    }

    /**
     * Add item into shopping cart
     * @param shoppingCart
     */
    public void addToCart(ShoppingCart shoppingCart) {
        shoppingCartDAO.addItemIntoCart(shoppingCart);
    }

    /**
     * This returns the list of shopping cart
     */
    @RequestMapping("/shoppingcart")
    public void getShoppingCartList(){

        List<ShoppingCart> cart = shoppingCartDAO.getShoppingList();
        List<Items> items = itemDAO.getItems();
        final double[] totalCost = {0.0};

        System.out.println(" size "+cart.size() +"    "+items.size());
        cart.forEach(shoppingCart -> {
            totalCost[0] = totalCost[0] +  findItemPrice(shoppingCart.getId(),items, shoppingCart.getQuantity());
        });

        System.out.println("total cost :: "+totalCost[0]);

    }
    private double findItemPrice(long itemID, final List<Items> items, int qty){

        double priceValue = items.stream().filter(item -> item.getId() == itemID).findAny().get().getPrice();
        String itemName = items.stream().filter(item -> item.getId() == itemID).findAny().get().getItemName();
        System.out.println("prive of the item :: "+priceValue);
        double discountedValue = applyDiscounts(itemID,priceValue, qty);
        System.out.println("Item name: "+itemName+" , Price of Item : "+priceValue +" , Discounted price : "+ discountedValue);
        return discountedValue;
    }

    /**
     * Calculates discounts on the item
     * @param itemId
     * @param priceValue
     * @param qty
     * @return
     */
    public double applyDiscounts(final long itemId, final double priceValue, int qty){


        List<Promotions> promotions = promotionsDAO.getPromotionsList();
        promotions.forEach(promotion -> {
        });
        String discountStr = promotions.stream().filter(promotions1 -> promotions1.getId() == itemId).collect(Collectors.toList()).get(0).getDiscount();
        //Calculates percent discount on the item
        if (discountStr.contains("%")){
            String disValue = discountStr.substring(0,discountStr.indexOf('%'));
            double discount = Double.parseDouble(disValue)*priceValue/100;
            double discountedValue = priceValue-(discount);
            System.out.println(discountedValue);
            return discountedValue;
        }
        else
        {
            //Calculates 3 for 2 discounts on the item
            return dis3For2(priceValue,qty);
        }

    }

    /**
     * Calculates 3 for 2 discounts on the item
     * @param itemPrice
     * @param qty
     * @return
     */
    public double dis3For2(double itemPrice, int qty) {
        double discountedPrice=0.0;
        double multiples = (qty*10.0)/(3*10.0);

        if(multiples/((int)(multiples))==1){
            discountedPrice = multiples*itemPrice;
            System.out.println("non decimals :: "+(int)multiples);
        }
        else
        {
            discountedPrice = multiples*itemPrice+itemPrice;
            System.out.println("discountedPrice : " +discountedPrice);
        }


        return discountedPrice;
    }
}
