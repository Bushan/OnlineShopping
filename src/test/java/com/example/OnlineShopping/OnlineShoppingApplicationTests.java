package com.example.OnlineShopping;

import com.example.OnlineShopping.items.ItemsControllerTest;
import com.example.OnlineShopping.promotions.PromotionsTest;
import com.example.OnlineShopping.shoppingcart.ShoppingCartTest;
import org.junit.Test;
import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OnlineShoppingApplicationTests {

	@Test
	public void contextLoads() {

		JUnitCore junit = new JUnitCore();
		junit.addListener(new TextListener(System.out));
		junit.run(ItemsControllerTest.class);
		junit.run(PromotionsTest.class);
		junit.run(ShoppingCartTest.class);

		Result result = junit.run(
				ItemsControllerTest.class,
				PromotionsTest.class,ShoppingCartTest.class);

		resultReport(result);
	}

	public static void resultReport(Result result) {
		System.out.println("Finished. Result: Failures: " +
				result.getFailureCount() + ". Ignored: " +
				result.getIgnoreCount() + ". Tests run: " +
				result.getRunCount() + ". Time: " +
				result.getRunTime() + "ms.");
	}

}
