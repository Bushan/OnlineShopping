package com.example.OnlineShopping.items;

import com.example.OnlineShopping.Controllers.ItemsController;
import com.example.OnlineShopping.dao.ItemDAO;
import com.example.OnlineShopping.dao.PromotionsDAO;
import com.example.OnlineShopping.dao.ShoppingCartDAO;
import com.example.OnlineShopping.domains.Items;
import com.example.OnlineShopping.domains.Promotions;
import com.example.OnlineShopping.domains.ShoppingCart;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;


@SpringBootTest
public class ItemsControllerTest {

    @Mock
    ItemDAO itemDAO;
    @Mock
    ShoppingCartDAO shoppingCartDAO;
    @Mock
    PromotionsDAO promotionsDAO;

    @Autowired
    ItemsController itemC;

    @Before
    public void setup(){

        MockitoAnnotations.initMocks(this);

        //Mocking the ItemDAO object to test the item controller
        Mockito.when(itemDAO.getItems()).thenReturn(prepareItemsList());
        //Mocking the ShoppingCartDao object to test the item controller
        Mockito.when(shoppingCartDAO.getShoppingList()).thenReturn(prepareShoppingList());
        // Mocking the PromotionsDAO object to test the item controller
        Mockito.when(promotionsDAO.getPromotionsList()).thenReturn(preparePromotions());
    }

    /**
     * Test data for the shopping cart
     * @return
     */
    private List<ShoppingCart> prepareShoppingList(){
        List<ShoppingCart> shoppingList = new ArrayList<>();
        shoppingList.add(new ShoppingCart(1001,"Headphones",1));
        shoppingList.add(new ShoppingCart(1002,"Speakers",1));

        return shoppingList;
    }

    /**
     * Test data for the promotion
     * @return
     */
    private List<Promotions> preparePromotions(){
        List<Promotions> promotionsList = new ArrayList<>();
        promotionsList.add(new Promotions(1001,"Audio","30% discount"));
        promotionsList.add(new Promotions(1002,"AAA Batteries","3 for 2"));

        return promotionsList;
    }
    /**
     * Test data for the list of items
     * @return
     */
    private List<Items> prepareItemsList(){
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(new Items(1001,"Headphones","Audio", 150.00));
        itemsList.add(new Items(1002,"Speakers","Audio", 85.00));
        itemsList.add(new Items(1003,"AA Batteries","Power", 3.00));
        itemsList.add(new Items(1004,"Protein","Food", 25.00));

        return itemsList;
    }

    @Test
    public void testGetShoppingCartList(){
         itemC = new ItemsController();
         itemC.setItemDAO(itemDAO);
         itemC.setShoppingCartDAO(shoppingCartDAO);
         itemC.setPromotionsDAO(promotionsDAO);
        itemC.getShoppingCartList();
    }

    @Test
    public void testDiscount() {
        itemC = new ItemsController();
        itemC.setPromotionsDAO(promotionsDAO);
        double discountedValue = itemC.applyDiscounts(1001,4,5);
        assertEquals(2.8,discountedValue);
    }
    @Test
    public void testDiscount3For2() {
        itemC = new ItemsController();
        itemC.setPromotionsDAO(promotionsDAO);
        double discountValue = itemC.applyDiscounts(1002,12,5);
        assertEquals(32.00,discountValue);
    }

}
