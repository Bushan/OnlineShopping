# Java 8 TDD 

Technologies:
  - Java 8
  - Spring boot
  - Junit and Mockito
  - 
  
Steps:
 - Unzip the project OnlineShopping
 - Import the project in IntelliJ or any other tool.
 - Execute mvn clean install command either in Intellij terminal or command prompt
 - open the class ItemsControllerTest and run the test case.
 - The data is hard coded in the DAO class.
 - 
 

